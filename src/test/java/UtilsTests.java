import org.junit.Assert;
import org.junit.Test;
import ro.wantsome.Utils;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class UtilsTests {
    @Test
    public void testPalindromesFinderIdentifiesPalindromes() {
        Set<String> inputSet = new HashSet<>();
        inputSet.add("asa");
        Set<String> palindromes = Utils.findPalindromes(inputSet);
        Assert.assertTrue(palindromes.contains("asa"));
    }

    @Test
    public void testPalindromesFinder_FindsNothing() {
        Set<String> inputSet = new HashSet<>();
        inputSet.add("acasa");
        Set<String> palindromes = Utils.findPalindromes(inputSet);
        Assert.assertEquals(0, palindromes.size());


    }

    @Test
    public void testPalindromesFinder_FindsMultiple() {
        Set<String> inputSet = new HashSet<>();
        inputSet.add("asa");
        inputSet.add("ata");
        inputSet.add("cojoc");
        inputSet.add("penar");
        Set<String> palindromes = Utils.findPalindromes(inputSet);
        Assert.assertEquals(3, palindromes.size());
    }

    @Test
    public void testPalindromesFinder_IsResilient() {
        Set<String> palindromes = Utils.findPalindromes(null);
        Assert.assertEquals(0, palindromes.size());
    }

    @Test
    public void testRandomWordFinder() {
        Set<String> inputSet = new HashSet<>();
        inputSet.add("1");
        inputSet.add("2");
        inputSet.add("3");
        inputSet.add("4");

        String randomWord = Utils.getRandomWordFromSet(inputSet, new Random(1L));
        Assert.assertEquals("3", randomWord);
    }
}
