package dictionaryoperations;

import ro.wantsome.Utils;

import java.io.IOException;
import java.util.Set;

public class RandomWordDictionaryOperation implements DictionaryOperation {

    private Set<String> wordsSet;

    public RandomWordDictionaryOperation(Set<String> wordsSet) {
        this.wordsSet = wordsSet;
    }

    @Override
    public void run() throws IOException {
        System.out.println("Random word");

        String randomWord = Utils.getRandomWord(wordsSet);

        System.out.println(randomWord);
    }
}
