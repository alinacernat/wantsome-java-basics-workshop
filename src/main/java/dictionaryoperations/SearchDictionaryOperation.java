package dictionaryoperations;

import ro.wantsome.Utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;
import java.util.Set;

public class SearchDictionaryOperation implements DictionaryOperation {
    private BufferedReader in;
    private Set<String> wordsSet;

    public SearchDictionaryOperation(Set<String> wordsSet, BufferedReader in) {
        this.in = in;
        this.wordsSet = wordsSet;
    }

    @Override
    public void run() throws IOException {
        System.out.println("Searching");
        String userGivenWord = in.readLine();

        if ("".equals(userGivenWord))
            return;
        Set<String> resultedWords =
                Utils.findsWordsThatContain(wordsSet, userGivenWord);

        List<String> sortedWords = Utils.sortSet(resultedWords);

        for (String line : sortedWords) {
            System.out.println(line);

        }
    }
}
