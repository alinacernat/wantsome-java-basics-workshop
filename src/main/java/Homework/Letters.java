package Homework;

import java.util.Arrays;

public class Letters {
    public static void main(String[] args) {
        String word = "sofia";
        char ch[] = word.toCharArray();
        Arrays.sort(ch);
        String sortedWord = new String(ch);
        System.out.println(sortedWord);
    }
}
