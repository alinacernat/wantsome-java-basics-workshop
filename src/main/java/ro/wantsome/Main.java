package ro.wantsome;

import dictionaryoperations.DictionaryOperation;
import dictionaryoperations.PalindromeDictionaryOperation;
import dictionaryoperations.RandomWordDictionaryOperation;
import dictionaryoperations.SearchDictionaryOperation;
import filereader.InputFileReader;
import filereader.ResourceInputFileReader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

//curatam setul de duplicate
public class Main {

    public static void main(String[] args) throws IOException {
        List<String> allLines = new ArrayList<>();

        InputFileReader inputFileReader = new ResourceInputFileReader();
        allLines = inputFileReader.readFile("dex.txt");

        Set<String> wordsSet = Utils.removeDuplicates(allLines);

        // Using Scanner for Getting Input from User
        BufferedReader in =
                new BufferedReader(new InputStreamReader(System.in));

        while (true) {

            System.out.println("Menu:\n\t" +
                    "1. Search\n\t" +
                    "2. Palindromes\n\t" +
                    "3. Random word\n\t"+
                    "0. Exit");

            String userMenuSelection = in.readLine();

            if (userMenuSelection.equals("0")) {
                System.out.println("Exit");
                break;
            }

            DictionaryOperation operation = null;

            switch (userMenuSelection) {
                case "1":
                    operation = new SearchDictionaryOperation(wordsSet, in);
                    break;
                case "2":
                    operation = new PalindromeDictionaryOperation(wordsSet);
                    break;
                case "3":
                    operation = new RandomWordDictionaryOperation(wordsSet);
                    break;
                default:
                    System.out.println("Please select a valid option");

            }

            if (operation != null) {
                operation.run();
            }
        }
    }

    public static void findsWordsThatContain(Set<String> wordsSet, String word) {
        for (String line : wordsSet) {
            if (line.contains(word))
                System.out.println(line);
        }
    }

    public static Set<String> removeDuplicates(List<String> allLines) {
        Set<String> wordsSet = new HashSet<>();
        for (String line : allLines) {
            wordsSet.add(line);
        }
        return wordsSet;
    }
}


